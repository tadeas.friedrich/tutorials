var readline = require('readline');
var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
var lines = [];

rl.prompt();
rl.on('line', function(line) {
    lines.push(line);
    rl.prompt();
}).on('close', function() {
    console.log(lines);
    process.exit(0);
});