/**
 * Created by svagrpa1 on 5/4/17.
 */

function Article (id, author, content, comments){
    this.id = id;
    this.author = author;
    this.content = content;
    this.comments = comments;
}

function Comment (id, author, article, content) {
    this.id = id;
    this.author = author;
    this.article = article;
    this.content = content;
    article.comments.push(this);
}

function serialize(data) {
    //Pole pro nacházení shod v již serializovaných objektech
    seen = [];

    //JSON springify s definicí replacer function - Pokud narazíme na již dříve
    // serializovaný objekt, vymeníme jeho serializaci za jeho id.
    var json = JSON.stringify(data, function(key, val) {
        if (typeof val == "object") {
            if (seen.indexOf(val) >= 0){
                return val.id;
            }
            seen.push(val);
        }
        return val;
    });
    return json;
}

var article = new Article(1,'Admin', 'Hello world!', new Array());
var comment1 = new Comment(1, 'YoungPadawan', article, 'Too simple!');
var comment2 = new Comment(2, 'OldPadawan', article, 'So boring!');

var aJSON = serialize(article);
var cJSON = serialize(comment1);
console.log(aJSON);
console.log(cJSON);